# Polylux SNS template
Copy `sns-theme.typ` and the `fonts/` dir in your project folder, then run in your shell
```
$ source ./env.sh
$ typst watch <INPUT>
```

In your source file, import
```
#import "@preview/polylux:0.3.1": *
#import "./sns-theme.typ": *
```
and launch
```
#show: sns-theme.with([OPTIONS])
```

## Supported `sns-theme` options
+ `aspect-ratio` (default: `16-9`). Also supported `4-3`.
+ `title` (default: `none`): presentation title.
+ `subtitle` (default: `none`): presentation subtitle.
+ `event` (default: `none`): presentation date and institute.
+ `short-title` and `short-event` (default: `title` and `event`, respectively).
+ `logo-1` (default: `none`). The logo must have a light color.
+ `logo-2` (default: `none`). The logo must have a dark color.
+ `authors` (default: `none`). It must be an array. Its elements are placed with a small horizontal displacement.

## Supported functions
+ `title-slide(body: none)`. It produces the title slide. That doesn't affect page counter.
+ `slide(title: none, subtitle: none, new-sec: false, body)`. If `new-sec` is `true`, a new section with the name `title` is created. If `new-sec` is a string/a content, a new section
is created and its name is `new-sec`.
+ `focus-slide(new-sec: none, body)`. It produces a focus slide. If `new-sec` is not `none`, a new section with the name `new-sec` is created.
+ `toc-slide()`. It produces the Table of Contents. That doesn't affect page counter.
+ `new-section-slide(name)`. It creates a new section which name is `name`. That doesn't affect page counter.
+ `empty-slide(body)`. It creates a new empty slide. That doesn't affect page counter.
