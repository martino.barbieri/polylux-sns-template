/***************************************************************
 * A polylux theme by martino barbieri (https://mbarbieri.it)  *
 *     Un bel giorno mi è partito il cusu e ho deciso          *
 *     di scrivere questo template. Lo carico giusto per       *
 *     sprecare un po' di spazio su GitLab :D                  *
 ***************************************************************/

#import "@preview/polylux:0.3.1": *

/* PER TEMPLATE SNS */
// Background and decorations
#let sns-theme_background-color      = rgb("#ffffff")
#let sns-theme_color-1               = rgb("#00728D")
#let sns-theme_color-2               = rgb("#183F56")
#let sns-theme_color-3               = rgb("#4D4E4F")
#let sns-theme_color-3               = rgb("#4D4E4F")
#let sns-theme_color-4               = rgb("#AB3502")
#let sns-theme_color-5               = rgb("#E69426")
#let sns-theme_color-6               = rgb("#9CD7F3")
#let sns-theme_logo-2                = state("logo-2", none)
#let sns-theme_logo-1                = state("logo-1", none)

/* PER TEMPLATE UNIPI */
/*
#let sns-theme_background-color      = rgb("#ffffff")
#let sns-theme_color-1               = cmyk(100%,57%,0%,9%)
#let sns-theme_color-2               = cmyk(100%,57%,0%,38%)
#let sns-theme_color-3               = rgb("#444444")
#let sns-theme_color-6               = cmyk(25%,7%,0%,0%)
#let sns-theme_logo-2                = state("logo-2", none)
#let sns-theme_logo-1                = state("logo-1", none)
*/

// Text and Fonts
#let sns-theme_text-font             = "Roboto"
#let sns-theme_title-font            = "Raleway"
#let sns-theme_main-text-color       = black
#let sns-theme_second-text-color     = white
#let sns-theme_title-text-color      = rgb("#444444")
#let sns-theme_size                  = 20pt

// Data
#let sns-theme_title                 = state("title",       none)
#let sns-theme_short-title           = state("short-title", none)
#let sns-theme_subtitle              = state("subtitle",    none)
#let sns-theme_event                 = state("event",       none)
#let sns-theme_short-event           = state("short-event", none)
#let sns-theme_authors               = state("authors",     none)

// Internals
#let sns-theme_sections              = state("sections", ())

// Init
#let sns-theme(
  // General parameters
  aspect-ratio : "16-9",

  // Data
  title        : none,
  subtitle     : none,
  short-title  : none,
  event        : none,
  short-event  : none,
  logo-2       : none,
  logo-1       : none,
  authors      : none,

  // Stuff
  body
) = {
  set page(
    paper : "presentation-" + aspect-ratio,
    fill  : sns-theme_background-color,
    margin: 0pt,
    header: none,
    footer: none,
    header-ascent: 0pt,
    footer-descent: 0pt,
  )
  set text(
    fill : sns-theme_main-text-color,
    size : sns-theme_size,
    font : sns-theme_text-font,
  )
  sns-theme_title.update(title)
  sns-theme_subtitle.update(subtitle)
  if short-title != none { sns-theme_short-title.update(short-title) }
    else { sns-theme_short-title.update(title) }
  sns-theme_event.update(event)
  if short-event != none { sns-theme_short-event.update(short-event) }
    else { sns-theme_short-event.update(event) }
  sns-theme_logo-2.update(logo-2)
  sns-theme_logo-1.update(logo-1)
  sns-theme_authors.update(authors)

  body
}

//Slides
#let title-slide(
  body            : none
) = {
  let content = align(top + center, context( {
    let title       = sns-theme_title.at(here())
    let subtitle    = sns-theme_subtitle.at(here())
    let event       = sns-theme_event.at(here())
    let authors     = sns-theme_authors.at(here())
    let logo        = sns-theme_logo-1.at(here())

    // Background
    place(
      bottom + right,
      polygon(
        fill   : sns-theme_color-3,
        stroke : none,
        (0%, 42%),(100%, 42%),(100%, 0%),
      ),
    )
    place(
      bottom + right,
      polygon(
        fill   : sns-theme_color-2,
        stroke : none,
        (0%, 29%),(0%, 37%),(100%, 37%),(100%, 0%),
      ),
    )
    place(
      bottom + right,
      polygon(
        fill   : sns-theme_color-1,
        stroke : none,
        (0%, 20%),(0%, 30%),(100%, 30%),(100%, 0%),
      ),
    )
    place(
      top + left,
      dx: 2pt,
      polygon(
        fill   : sns-theme_color-3,
        stroke : none,
        (10%, 0%),(0%, 16%),(75%, 0%),
      ),
    )
    place(
      top + left,
      polygon(
        fill   : sns-theme_color-1,
        stroke : none,
        (0%, 0%),(0%, 15%),(75%, 0%),
      ),
    )
    place(
      bottom + center,
      dy: -1.2em,
      text(fill: sns-theme_background-color, size: 22pt, event)
    )
    if logo != none {
      if type(logo) != str {
        logo
      } else {
        place(
          bottom + right,
          dx: -0.75em,
          dy: -0.5em,
          image(logo, height: 22%),
        )
      }
    }

    // Contents
    set align(center + horizon)
    v(-25%)
    text(
      font : sns-theme_title-font,
      size : 64pt,
      fill: sns-theme_title-text-color,
      strong(title)
    )
    linebreak()
    text(
      font : sns-theme_title-font,
      size : 22pt,
      fill: sns-theme_title-text-color,
      subtitle
    )
    line(length: 90%, stroke: (paint: sns-theme_color-6, thickness: 4pt))
    v(0.5em)
    set text(size: 24pt, top-edge: 0pt, bottom-edge: 0pt, fill: sns-theme_main-text-color)
    for i in range(authors.len()) {
      move(dx: 2% * ( i - ( authors.len() - 1 ) / 2 ), authors.at(i))
    }
    body
  } ))

  polylux-slide({ content })

  logic.logical-slide.update( x => x - 1 )
}

#let small-sections(show-sec-name: true) = context( {
  let sections    = sns-theme_sections.at(here())
  if (sections.len() > 0 and show-sec-name) {sections.last();" "}
  for i in range(sections.len()-1) {$circle.filled$}
  if sections.len() > 0 {$ast.circle$}
  for i in range(sections.len(),sns-theme_sections.final().len()) {$circle.dotted$}
} )

#let big-sections = context( {
  set align(horizon + right)
  let sections    = sns-theme_sections.at(here())
  grid(
  box(height: 40%, inset: 1em)[#{
    for i in range(sections.len()-1) {$circle.filled$}
    if sections.len() > 0 {$ast.circle$}
    for i in range(sections.len(),sns-theme_sections.final().len()) {$circle.dotted$}
  }],
  box(height: 60%, inset: 1em)[
    #if sections.len() > 0 { sections.last() }
  ])
} )

#let new-section(name) = {
    utils.register-section(name)
    sns-theme_sections.update( x => { x.push(name); x } )
}

#let slide(
  title           : none,
  subtitle        : none,
  new-sec         : false,
  body
) = {
  // HEADER
  let header = align(top, context( {
    let logo        = sns-theme_logo-2.at(here())

    // If new-sec is false, do nothing
    // If new-sec is true, new section's name is the slide title
    // Else the new-sec name is new-sec
    if type(new-sec)==bool {if new-sec {new-section(title)}}
    else {new-section(new-sec)}

    place(
      top + left,
      grid(
        columns: (auto,auto),
        gutter: 0pt,
        if logo != none { if type(logo) == str { pad(image(logo, height: 100%), x: 0.7em, top: 0.5em) }
          else {logo} },
        align(horizon+right,
        if title != none {
          grid(rect(
            width: 100%, height: 40%,
            inset: 1em, outset: 0pt,
            fill: sns-theme_color-2,
            align(horizon,text(fill: sns-theme_second-text-color, {
              small-sections(show-sec-name: new-sec != true)}
            ))
          ),
          rect(
          width: 100%, height: 60%,
          inset: 1em, outset: 0pt,
          fill: sns-theme_color-1,
          align(horizon + center,
          {
            text(
              fill: sns-theme_second-text-color,
              font: sns-theme_title-font,
              size: 32pt,
              weight: "bold",
              title
            )
            if subtitle != none {
              smallcaps(text(
                fill: sns-theme_second-text-color,
                font: sns-theme_title-font,
                size: 24pt,
                " — " + subtitle
              ))
            }
          }
          )
          ))} else {
          rect(
            width: 100%, height: 100%,
            inset: 0em, outset: 0pt,
            fill: sns-theme_color-2,
            align(horizon + right,text(fill: sns-theme_second-text-color,
              big-sections
            )))
          }
        )
      )
    )
  } ))

  // FOOTER
  let footer = align(top + center, context( {
    let short-title = sns-theme_short-title.at(here())
    let short-event = sns-theme_short-event.at(here())

    place(top + center, line(length: 100%-1.4em, stroke: (paint: sns-theme_color-2, thickness: 2pt)))

    block(
      height: 100%,
      width: 100%,
      inset: (x: 1em),
      align(horizon,text(fill: sns-theme_color-3, size: 16pt,
      grid(
        gutter: 1em,
        columns: (0.7fr, 1fr, 0.7fr),
        align(left, short-title),
        align(center, smallcaps(short-event)),
        align(right, [#logic.logical-slide.display()/#utils.last-slide-number]),
      ))
    ))
  } ))

  set page(
    margin: (top: 3.2em, bottom: 1.1em),
    header: header,
    footer: footer,
  )

  polylux-slide({
    set align(horizon)
    set text(size: sns-theme_size, top-edge: 20pt, bottom-edge: 0pt)
    show: block.with(inset: (x: 1.5em, y:.3em), width: 100%)
    body
  })
}

#let focus-slide(
  new-sec : none,
  body
) = {
  // HEADER
  let header = align(top, context( {
    let logo        = sns-theme_logo-1.at(here())

    // If new-sec is none, do nothing
    // Else the new-sec name is new-sec
    if new-sec!=none {new-section(new-sec)}

    place(
      top + left,
      grid(
        columns: (auto,auto),
        gutter: 0pt,
        if logo != none { if type(logo) == str { pad(image(logo, height: 100%), x: 0.7em, top: 0.5em) }
          else {logo} },
        align(top+right,grid(
          box(
            width: 100%, height: 100%,
            inset: 0em, outset: 0pt,
            align(horizon,text(fill: sns-theme_second-text-color,
              big-sections
            ))
          ),
        )
      ))
    )
  } ))

  set page(
    margin: (top: 3.2em, bottom: 0em),
    header: header,
    footer: none,
    fill: sns-theme_color-2,
  )

  polylux-slide({
    set align(horizon + center)
    set text(
      size: sns-theme_size*1.5,
      fill: sns-theme_second-text-color,
      style: "italic"
    )
    show: block.with(inset: (x: 1.5em, y:.3em), width: 100%)
    body
  })
}

#let empty-slide(
  body
) = {
  // HEADER
  let header = align(top, context( {
    let logo        = sns-theme_logo-1.at(here())

    place(
      top + left,
      box(
        height: 3.2em,
        if logo != none { if type(logo) == str { pad(image(logo, height: 100%), x: 0.7em, top: 0.5em) }
          else {logo} }
      )
    )
  } ))

  set page(
    margin: (top: 0em, bottom: 0em),
    foreground: header,
    footer: none,
    fill: sns-theme_color-2,
  )

  polylux-slide({
    set align(horizon + center)
    set text(
      size: sns-theme_size*1.5,
      fill: sns-theme_second-text-color,
    )
    show: block.with(inset: (x: 1.5em, y:.3em), width: 100%)
    body
  })

  logic.logical-slide.update( x => x - 1 )
}

#let new-section-slide(name)  = {
  // HEADER
  let header = align(top, context( {
    let logo        = sns-theme_logo-1.at(here())

    place(
      top + left,
      box(
        height: 3.2em,
        if logo != none { if type(logo) == str { pad(image(logo, height: 100%), x: 0.7em, top: 0.5em) }
          else {logo} }
      )
    )
  } ))

  set page(
    margin: (top: 0em, bottom: 0em),
    foreground: header,
    footer: none,
    fill: sns-theme_color-2,
  )

  // TOC
  let content = context( {
    let sections    = sns-theme_sections.at(here())
    set text(weight: "regular")
    for i in range(sections.len()-1) [
      + #sections.at(i)
    ]
    [
      + #text(weight: "black", sections.last())
    ]
    set text(weight: "thin")
    for i in range(sections.len(),sns-theme_sections.final().len()) [
      + #sns-theme_sections.final().at(i)
    ]
  } )

  polylux-slide({
    set align(horizon + center)
    set text(
      size: sns-theme_size*1.5,
      fill: sns-theme_second-text-color,
    )
    new-section(name)

    show: box.with()
    set align(left)
    content
  })

  logic.logical-slide.update( x => x - 1 )
}

#let toc-slide()  = {
  // HEADER
  let header = align(top, context( {
    let logo        = sns-theme_logo-1.at(here())

    place(
      top + left,
      box(
        height: 3.2em,
        if logo != none { if type(logo) == str { pad(image(logo, height: 100%), x: 0.7em, top: 0.5em) }
          else {logo} }
      )
    )
  } ))

  set page(
    margin: (top: 0em, bottom: 0em),
    foreground: header,
    footer: none,
    fill: sns-theme_color-2,
  )

  // TOC
  let content = context( {
    let sections    = sns-theme_sections.final()
    set text(weight: "regular")
    for i in sections [
      + #i
    ]
  } )

  polylux-slide({
    set align(horizon + center)
    set text(
      size: sns-theme_size*1.5,
      fill: sns-theme_second-text-color,
    )

    show: box.with()
    set align(left)
    content
  })

  logic.logical-slide.update( x => x - 1 )
}
